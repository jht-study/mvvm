//
//  ListViewController.swift
//  MVVM
//
//  Created by Administrador on 11/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, UITableViewDataSource {
    private let cellIdentifier = "infoCell"
    private let tableView = UITableView()
    private let headerVM = HeaderViewModel()
    
    func setupTableView() {
        self.tableView.register(HeaderCell.self, forCellReuseIdentifier: cellIdentifier)
        self.tableView.rowHeight = 50
        self.view.addSubview(self.tableView)
        
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.tableView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.tableView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        self.tableView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        self.tableView.heightAnchor.constraint(equalTo: self.view.heightAnchor).isActive = true
        
        self.tableView.dataSource = self
    }
    
    override func loadView() {
        let view = UIView()
        view.backgroundColor = .white
        self.view = view
        self.headerVM.updateList = {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        self.setupTableView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.headerVM.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier) as! HeaderCell
        let cellVM = self.headerVM.cellVM(forIndex: indexPath.row)
        cell.setup(viewModel: cellVM)
        return cell
    }
}
