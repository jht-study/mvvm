//
//  HeaderCell.swift
//  MVVM
//
//  Created by Administrador on 11/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {
    
    func setup(viewModel: CellViewModel) {
        self.textLabel?.text = viewModel.description()
    }
    
}
