//
//  ViewController.swift
//  MVVM
//
//  Created by Administrador on 10/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import UIKit

class GreetingViewController: UIViewController {
    @IBOutlet var showGreetingButton: UIButton!
    @IBOutlet var greetingLabel: UILabel!
    
    var viewModel: GreetingViewModelProtocol! {
        didSet {
            self.viewModel.greetingDidChange = { [unowned self] viewModel in
                self.greetingLabel.text = viewModel.greeting
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.showGreetingButton.addTarget(self.viewModel, action: #selector(didTapButton(sender:)), for: .touchUpInside)
        
        let model = Person(firstName: "Jeferson", lastName: "Takumi")
        viewModel = GreetingViewModel(person: model)
    }
    
    @objc func didTapButton(sender: UIButton) {
        guard let vm = self.viewModel else { return }
        vm.showGreeting()
    }
}

