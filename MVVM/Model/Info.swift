//
//  Info.swift
//  MVVM
//
//  Created by Administrador on 11/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

public struct Info {
    let title: String
    let detail: String
    
    public func description() -> String {
        return "\(title): \(detail)"
    }
}
