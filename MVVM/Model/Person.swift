//
//  Person.swift
//  MVVM
//
//  Created by Administrador on 10/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

struct Person {
    let firstName: String
    let lastName: String
}
