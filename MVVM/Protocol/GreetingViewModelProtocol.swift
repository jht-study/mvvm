//
//  GreatingViewModelProtocol.swift
//  MVVM
//
//  Created by Administrador on 10/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

protocol GreetingViewModelProtocol: class {
    var greeting: String? { get }
    var greetingDidChange:((GreetingViewModelProtocol) -> ())? { get set }
    init(person: Person)
    func showGreeting()
}
