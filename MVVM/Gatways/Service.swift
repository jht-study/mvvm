//
//  Service.swift
//  MVVM
//
//  Created by Administrador on 11/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation

class Service {
    private var data:[Info]!
    
    func fecthInfo(update: @escaping([Info]) -> ()) {
        guard let url = URL(string: "https://httpbin.org/headers") else { return }
        let sessionTask = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if let jsonData = data, let json = try? JSONSerialization.jsonObject(with: jsonData
                , options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject] {
                DispatchQueue.main.async {
                    self.data = []
                    if let headers = json["headers"] as? [String : String] {
                        for (key, value) in headers {
                            self.data.append(Info(title: key, detail: value))
                        }
                    }
                    update(self.data)
                }
            }
        }
        sessionTask.resume()
    }
}
