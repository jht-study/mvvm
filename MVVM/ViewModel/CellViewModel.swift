//
//  CellViewModel.swift
//  MVVM
//
//  Created by Administrador on 11/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation

class CellViewModel {
    private let info: Info!
    
    init(_ info: Info) {
        self.info = info
    }
    
    public func description() -> String {
        return self.info.description()
    }
}
