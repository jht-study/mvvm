//
//  GreetingViewModel.swift
//  MVVM
//
//  Created by Administrador on 10/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

class GreetingViewModel: GreetingViewModelProtocol {
    let person: Person
    var greeting: String? {
        didSet{
            print(self)
            self.greetingDidChange?(self)
        }
    }
    var greetingDidChange: ((GreetingViewModelProtocol) ->())?
    
    required init(person: Person) {
        self.person = person
    }
    
    func showGreeting() {
        self.greeting = "Hello" + " " + self.person.firstName + " " + self.person.lastName
    }
}
