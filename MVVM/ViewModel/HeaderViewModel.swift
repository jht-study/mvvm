//
//  HeaderViewModel.swift
//  MVVM
//
//  Created by Administrador on 11/01/2019.
//  Copyright © 2019 Takumi. All rights reserved.
//

import Foundation

public typealias UpdatedClosure = () -> ()

class HeaderViewModel {
    private let service = Service()
    public var updateList: UpdatedClosure?
    private var data:[CellViewModel] = [] {
        didSet{
            DispatchQueue.main.async {
                self.updateList?()
            }
        }
    }
    
    private func tryFechData() {
        self.service.fecthInfo {
            self.data = $0.map { CellViewModel($0) }
        }
    }
    
    public func numberOfRows() -> Int {
        let rows = self.data.count
        if rows == 0 {
            self.tryFechData()
        }
        return data.count
    }
    
    public func cellVM(forIndex index: Int) -> CellViewModel {
        if index < self.data.count {
            return self.data[index]
        }
        return CellViewModel(Info(title: "", detail: ""))
    }
    
}
